import sys
from malicious import create_app

sys.path.insert(0, '/var/www/malicious')

application = create_app()