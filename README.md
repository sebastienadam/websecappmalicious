# WebSecAppMalicious

Malicious web application for web application security course.

## Preamble

The application must be installed in the `/var/www/malicious` directory.

## Creating virtual environment

To create the Python Virtual Environment, proceed like this:

```bash
python3 -m venv venv
. venv/bin/activate
pip install -U pip wheel setuptools
pip install -r requirements.txt
```

## Installation

Create a `instance` folder with a `app.cfg` containing:

```
APP_NAME="Malicious Web App"
SECRET_KEY="<this_is_my_secret_key_to_secure_cookies>"
TARGET_FILE="/home/<user>/malicious.log"
UNSECURED_URL="<unsecured>"
```

Replace `<user>` with the username of the one who will receive the information. Replace `<unsecured>` with the URL of the unsecured web site.

## Generate self-signed certificate

Run following command filling all fields:

```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-malicious.key -out /etc/ssl/certs/apache-malicious.crt
```

## Configure Apache server

Install and activate `wsgi` module.

Create a `logs` directory into base directory.

Copy file `malicious.conf` into `/etc/apache2/sites-available`.

Run commands:

```sh
sudo a2enmod ssl               # activate https
sudo a2ensite malicious        # activate malicious web site
sudo systemctl reload apache2  # restart apache sever
```

## Configure domain name

Add following line into `/etc/hosts` file:

```
192.168.23.1	malicious.local malicious
```

You can now access Malicious web application through address https://malicious.local/

-------

*&copy; Sébastien Adam 2022 ~ 2024*
