from flask import Flask, render_template, request, send_file
from flask_cors import CORS
from pathlib import Path

from . import auth
from . import cookies
from . import errors
from . import js
from . import plugin

def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    CORS(app, supports_credentials=True)

    # ensure the instance folder exists
    Path(app.instance_path).mkdir(parents=True, exist_ok=True)

    # Load config
    app.config.from_pyfile('app.cfg', silent=True)

    # Managing errors
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(500, errors.internal_server_error)

    # Load modules
    app.register_blueprint(auth.bp)
    app.register_blueprint(cookies.bp)
    app.register_blueprint(js.bp)
    app.register_blueprint(plugin.bp)

    # main page
    @app.route('/')
    def index():
        return render_template('common/index.html')

    return app
