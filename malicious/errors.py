from flask import render_template
import traceback

def internal_server_error(e):
    return render_template('common/500.html', error_type=500, trace=traceback.format_exc()), 500

def page_not_found(e):
    return render_template('common/error.html', error_type=404), 404
