const content = document.evaluate(
    '/html/body/div[1]/article/p',
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null,
  ).singleNodeValue;

content.innerText = "Fake news!";

const link = document.evaluate(
    '/html/body/nav/a[3]',
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null,
  ).singleNodeValue;

link.setAttribute("href", "{base_url}auth/login");