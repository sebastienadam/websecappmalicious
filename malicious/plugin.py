from flask import Blueprint, abort, current_app, send_file
from pathlib import Path

bp = Blueprint('plugin', __name__, url_prefix='/plugin')

@bp.route('/<path:path>', methods=('GET', ))
def download(path):
    """
    return plugin files
    """
    print(path)
    if not path in ['webshell.html', 'webshell.py']:
        abort(404)
    file_path = Path(current_app.root_path) / 'data' / path
    return send_file(file_path, as_attachment=True)
